from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.http import JsonResponse

from .models import Profile
from .forms import SettingsForm

def logout_view(request):
    """Log a user out."""
    logout(request)
    return redirect('conversations:index')

def register(request):
    """Register a user."""
    if request.method != 'POST':
        # Display blank registration form.
        form = UserCreationForm()
    else:
        # Process completed form.
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            new_user = form.save()
            new_profile = Profile(user=new_user, display_name="")
            new_profile.save()
            
            login(request, new_user)
            return redirect('conversations:conversations')
    
    context = {'form': form}
    return render(request, 'registration/register.html', context)

@login_required
def settings(request):
    """Edit settings of a user."""
    # Make profile form.
    current_profile = Profile.objects.filter(user=request.user)[0]
    info = {"display_name":current_profile.display_name}
    settingsForm = SettingsForm(data=info)
    
    # Make password form.
    passwordForm = PasswordChangeForm(request.user)
    
    context = {'settingsForm': settingsForm, 'passwordForm': passwordForm}
    return render(request, 'registration/settings.html', context)

@login_required
@require_POST
def submit_profile(request):
    """Background process for settings: update profile."""
    current_profile = Profile.objects.filter(user=request.user)[0]
    # Update display name.
    current_profile.display_name = request.POST['display_name']
    # Update display picture if one was uploaded.
    try:
        current_profile.display_picture = request.FILES['display_picture']
    except KeyError:
        pass
    
    current_profile.save()
    return JsonResponse({})

@login_required
@require_POST
def submit_password(request):
    """Background process for settings: update password."""
    form = PasswordChangeForm(request.user, data=request.POST)
    if form.is_valid():
        user = form.save()
        update_session_auth_hash(request, user)
        response = JsonResponse({})
    else:
        response = JsonResponse({}, status=500)
        
    return response