"""Defines forms for users."""

from django import forms
from .models import Profile

class SettingsForm(forms.Form):
    display_name = forms.CharField(label="Display Name", required=False)
    display_picture = forms.ImageField(label="Update Display Picture", required=False)