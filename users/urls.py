"""Defines URL patterns for users."""

from django.urls import path, include

from . import views

app_name = 'users'
urlpatterns = [
    # Include default auth urls.
    path('', include('django.contrib.auth.urls')),
    
    # Logout page.
    path('logout_view/', views.logout_view, name='logout_view'),
    
    # Registration page.
    path('register/', views.register, name='register'),
    
    # User settings page.
    path('settings/', views.settings, name='settings'),
    path('settings/submit_profile/', views.submit_profile, name='submit_profile'),
    path('settings/submit_password/', views.submit_password, name='submit_password'),
]