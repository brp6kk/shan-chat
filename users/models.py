from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    """Profile of a user."""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=25)
    display_picture = models.ImageField(upload_to='shan_chat')
    
    def __str__(self):
        """Return a string representation of the model."""
        desc = ""
        if len(self.display_name) > 0:
            desc = desc + self.display_name + " "
        desc = desc + "(" + self.user.username + ")"
        
        return desc 