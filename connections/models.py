from django.db import models
from users.models import Profile
from django.contrib.auth.models import User

class Connection(models.Model):
    """User associated with another user."""
    connection = models.ForeignKey(Profile, on_delete=models.CASCADE)
    notes = models.TextField(blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        """Return a string representation of the model."""
        desc = str(self.connection)
        if len(self.notes) > 0:
            desc = desc + " - " + self.notes[:50] 
            if len(self.notes) > 50:
                desc += "..."
        
        return desc