"""Defines forms for connections."""

from django import forms
from django.contrib.auth.models import User
from users.models import Profile
from .models import Connection

class NewConnectionForm(forms.Form):
    """Create new connection."""
    username = forms.CharField(label="Username")
    notes = forms.CharField(label="Notes", required=False, widget=forms.Textarea)
    
    def is_valid(self, current_user):
        """Checks that form input is legal."""
        check = forms.Form.is_valid(self)
        if check:
            # Check if user with given username exists.
            username = self.data["username"]
            users = User.objects.filter(username=username)
            user_exists = len(users) > 0
            if not user_exists:
                self.add_error(field="username", error="Username not found.")
                return False
            
            # Check if current user already has connection with given user.
            existing = Connection.objects.filter(
                owner=current_user).filter(connection__user=users[0])
            connection_exists = len(existing) > 0
            if connection_exists:
                self.add_error(field="username", 
                    error="Connection already exists.")
                return False
        
        return check