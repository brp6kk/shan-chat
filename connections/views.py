from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.http import require_POST
from django.http import JsonResponse

from .models import Connection
from .forms import NewConnectionForm
from users.models import Profile

@login_required
def connections(request):
    """View all connections for a user."""
    connections = Connection.objects.filter(owner=request.user).order_by(
        'connection__display_name', 'connection__user__username')
    context = {'connections': connections}
    return render(request, 'connections/connections.html', context)

@login_required
@require_POST
def new_connection(request):
    """Create a new connection."""
    
    form = NewConnectionForm(data=request.POST)
    if form.is_valid(request.user):
        user = User.objects.filter(username=form.data["username"])[0]
        profile = Profile.objects.filter(user=user)[0]
        connection = Connection(connection=profile, 
                                notes=form.data["notes"], owner=request.user)
        connection.save()
    else: 
        return JsonResponse({}, status=500)
    
    result = { 'connectionId': connection.id, 'connection': str(connection) }
    
    return JsonResponse(result)

@login_required
@require_POST
def get_connection(request):
    """Get a connection."""
    try:
        connection = Connection.objects.get(id=int(request.POST['connectionId']))
    except:
        return JsonResponse({}, status=500)
    if connection.owner != request.user:
        return JsonResponse({}, status=500)
    
    result = { 'name': str(connection.connection), 'notes': connection.notes }
        
    return JsonResponse(result)

@login_required
@require_POST
def edit_connection(request):
    """Edit details of a connection."""
    try:
        connection = Connection.objects.get(id=int(request.POST['connectionId']))
    except:
        return JsonResponse({}, status=500)
    if connection.owner != request.user:
        return JsonResponse({}, status=500)
    
    connection.notes = request.POST['notes']
    connection.save()
    
    result = { 'connection': str(connection), 
               'name': str(connection.connection), 
               'notes': connection.notes }
    print(result)
    
    return JsonResponse(result)

@login_required
@require_POST
def delete_connection(request):
    """Delete a connection."""
    try:
        connection = Connection.objects.get(id=int(request.POST['connectionId']))
    except:
        return JsonResponse({}, status=500)
    if connection.owner != request.user:
        return JsonResponse({}, status=500)
    
    connection.delete()
    
    return JsonResponse({})