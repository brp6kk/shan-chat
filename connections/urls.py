"""Defines urls for connections."""

from django.urls import path, include

from . import views

app_name = 'connections'
urlpatterns = [
    # Connections home page.
    path('', views.connections, name='connections'),
         
    # Background process.
    path('get_connection/', views.get_connection, name='get_connection'),
    path('edit_connection/', views.edit_connection, name='edit_connection'),
    path('delete_connection/', views.delete_connection, name='delete_connection'),
    path('new_connection/', views.new_connection, name='new_connection'),
]