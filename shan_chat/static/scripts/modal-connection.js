$(function() {
    // Global variables.
    let $modal = $('div.modal-window.connection');
    let $content = $modal.children().filter('.modal-content');
    let connectionId;
    
    // Updates modal window to show connection details.
    // data: object containing name and notes properties.
    function showConnectionDetails(data) {
        $content.children().remove();
        
        let $connectionName = $('<h3>');
        $connectionName.attr('data-name', data.name);
        $connectionName.text(data.name);
        let $connectionNotes = $('<p>');
        $connectionNotes.attr('data-notes', data.notes);
        $connectionNotes.text(data.notes);
        
        let $connectionEdit = $('<a>');
        $connectionEdit.text("Edit connection"); 
        $connectionEdit.on('click', clickEdit);
        let $connectionDelete = $('<a>');
        $connectionDelete.text("Delete connection");
        $connectionDelete.on('click', clickDelete);
        
        $content.append($connectionName);
        $content.append($connectionNotes);
        $content.append($connectionEdit);
        $content.append($connectionDelete);
        
        $modal.show();
    }
    
    // Gets connection details and displays them in the modal window.
    function getConnection() {
        $.ajax({
            type: 'POST',
            url: 'get_connection/',
            data: { 'connectionId': connectionId },
            success: showConnectionDetails,
            error: function(data) {
                $content.append('<p>Error loading connection details.</p>');
                $modal.show();
            }
        });
    }
    
    // Edits connection details.
    // New notes are saved and
    // regular connection details content replaces edit modal.
    function editConnectionDetails(e) {
        e.preventDefault();
        let notes = $content.children().first().next().children().first().val();
        
        $.ajax({
            type: 'POST',
            url: 'edit_connection/',
            data: { 'notes': notes, 'connectionId': connectionId },
            success: function(data) {
                // Update contents of modal.
                // Update list item containing connection.
                showConnectionDetails(data);
                let $listItem = $("a[data-connection-id='" + connectionId + "']");
                $listItem.text(data.connection);
            },
            error: function(data) {
                $content.children().remove();
                $content.append('<p>Error editing connection details.</p>');
            }
        });
    }
    
    // Deletes connection details, removes connection from list,
    // and closes modal window.
    function deleteConnectionDetails(e) {
        e.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: 'delete_connection/',
            data: { 'connectionId': connectionId },
            success: function(data) {
                // Remove list item containing connection.
                // Clear and close modal.
                let $listItem = $("a[data-connection-id='" + connectionId + "']");
                $listItem.parent().remove();
                $modal.hide();
            },
            error: function(data) {
                $content.children().remove();
                $content.append('<p>Error editing connection details.</p>');
            }
        });
    }
    
    // Executes when a connection in the unordered list is clicked.
    // Uses getConnection to get & display connection details.
    function clickConnection(e) {
        e.preventDefault();
        connectionId = $(this).attr('data-connection-id');
        
        getConnection();
    }
    
    // Executes when "edit connection" is clicked in modal window.
    // Updates modal window to show a form to change connection notes.
    function clickEdit(e) {
        e.preventDefault();
        
        // Removes all elements except for connection name.
        let $connectionName = $content.children().first().detach();
        let notes = $content.children().first().text();
        $content.children().remove();
        $content.append($connectionName);
        
        // Create form and add to modal window.
        let $connectionForm = $('<form>');
        
        let $inputNotes = $('<textarea>');
        $inputNotes.val(notes);
        $connectionForm.append($inputNotes);
        
        let $editButton = $('<button>');
        $editButton.attr('id', 'submitEdit');
        $editButton.text('Done');
        $editButton.on('click', editConnectionDetails);
        $connectionForm.append($editButton);
        
        $content.append($connectionForm);
    }
    
    // Executes when "delete connection" is clicked in modal window.
    // Updates modal window to show form to confirm connection deletion.
    function clickDelete(e) {
        e.preventDefault();
        
        // Removes all elements except for connection name.
        let name = $content.children().first().text();
        let notes = $content.children().first().next().text();
        $content.children().remove();
        
        let $prompt = $('<h3>');
        $prompt.text('Are you sure you want to remove ' + name + 
                     ' from your connections?');
        $content.append($prompt);
        
        let $deleteForm = $('<form>');
        let $buttonYes = $('<button>');
        $buttonYes.text('Yes');
        $buttonYes.on('click', deleteConnectionDetails);
        $deleteForm.append($buttonYes);
        
        let $buttonNo = $('<button>');
        $buttonNo.text('Nevermind');
        $buttonNo.on('click', function(e) {
            e.preventDefault();
            showConnectionDetails({'name': name, 'notes': notes});
        });
        $deleteForm.append($buttonNo);
        
        $content.append($deleteForm);
    }
    
    $('ul').on('click', 'li a[data-connection-id]', clickConnection);
});