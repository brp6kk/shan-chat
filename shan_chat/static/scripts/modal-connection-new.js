$(function() {
    // Global variables.
    let $modal = $('div.modal-window.connection');
    let $content = $modal.children().filter('.modal-content');
    
    // Takes form data and attempts to create a connection.
    // On success, connection is added to list and modal is closed.
    // On failure, an error message is shown.
    function submitConnection(e) {
        e.preventDefault();
        
        let username = $('#input-username').val();
        let notes = $('#input-notes').val();
        
        $.ajax({
            type: 'POST',
            url: 'new_connection/',
            data: { 'username': username, 'notes': notes },
            success: function(data) {
                // Add new connection to top of list and hide modal.
                let $listItem = $('<li>');
                let $listContents = $('<a>');
                $listContents.attr('data-connection-id', data.connectionId);
                $listContents.text(data.connection);
                $listItem.append($listContents);
                $('ul#connection-list').prepend($listItem);
                $modal.hide();
            },
            error: function(data) {
                $content.prepend('<p>Error creating connection. ' +
                    'Connection may already be made or ' +
                    'user may not exist.</p>');
            }
        });
    }
    
    // Shows modal with form to create new connection.
    function clickNewConnection(e) {
        e.preventDefault();
        
        $content.children().remove();
        
        let $connectionForm = $('<form>');
        
        let $labelUsername = $('<label>');
        $labelUsername.attr('for', 'input-username');
        $labelUsername.text('Username:');
        let $inputUsername = $('<input>');
        $inputUsername.attr('id', 'input-username');
        $connectionForm.append($labelUsername);
        $connectionForm.append($inputUsername);

        let $labelNotes = $('<label>');
        $labelNotes.attr('for', 'input-notes');
        $labelNotes.text('Notes:');
        let $inputNotes = $('<textarea>');
        $inputNotes.attr('id', 'input-notes');
        $connectionForm.append($labelNotes);
        $connectionForm.append($inputNotes);
        
        let $submitButton = $('<button>');
        $submitButton.text('Create connection');
        $submitButton.on('click', submitConnection);
        $connectionForm.append($submitButton);
        
        $content.append($connectionForm);
        $modal.show();
    }
    
    $('#new-connection').on('click', clickNewConnection);
});