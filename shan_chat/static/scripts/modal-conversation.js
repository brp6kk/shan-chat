$(function() {
    // Global variables.
    let $modal = $('div.modal-window.conversation');
    let $content = $modal.children().filter('.modal-content');
    let conversationId;
    let $chatWindow;
    let $chatMessages;
    
    // Creates a li element based on the passed message.
    function createMessageElement(message) {
        let newMessage = $('<li>');
        newMessage.attr('data-message-id', message.id);
        newMessage.text(message.author_display_name + ": " + 
            message.content);
        return newMessage;
    }
    
    // Responds to scrolling of chat window.
    // If the user has scrolled to the top, tries loading in older messages.
    function scrollChatWindow(e) {
        if ($chatWindow.scrollTop() <= 0) {
            let oldestMessageId = $chatMessages.children()
                .first().attr('data-message-id');
            
            $.ajax({
                type: 'POST',
                url: 'get_older_messages/',
                data: { 
                    'conversationId': conversationId, 
                    'messageId': oldestMessageId
                },
                success: function(data) {
                    let index = 0;
                    let numMessages = data.messages.length;
                    let newMessage;
                    for ( ; index < numMessages; index++) {
                        newMessage = createMessageElement(data.messages[index]);
                        $chatMessages.prepend(newMessage);
                    }
                    
                    // Prevent automatic scrollup to top of older messages.
                    if (numMessages > 0) {
                        let scrollHeight = newMessage.height() * numMessages;
                        $chatWindow.scrollTop(scrollHeight);
                    }
                    
                }
            }); 
        }
    }
    
    // Adds new messages to the bottom of the message list.
    function addNewMessages(messages) {
        let index = messages.length - 1;
        for ( ; index >= 0; index--) {
            let messageExists = $('[data-message-id="' + messages[index].id 
                + '"]').length !== 0;
            if (!messageExists) {
                let newMessage = createMessageElement(messages[index]);
                $chatMessages.append(newMessage);
            }
        }
    }
    
    // Deletes old (not in the most recent 20) messages from the message list.
    function deleteOldMessages() {
        let numDelete = $chatMessages.children().length - 20;
        let index;
        for (index = 0; index < numDelete; index++) {
            $chatMessages.children().first().remove();
        }
    }
    
    // If the user does not send an amount, 
    // returns the number of unseen pixels below the scrollable area.
    // If the user sends an amount,
    // scrolls to the position so that there are amount unseen pixels
    // below the scrollable area.
    function scrollBottom(elem, amount) {
        let buffer = 17;
        if (typeof amount === 'number') {
            let scroll = elem[0].scrollHeight - amount - 
                elem.innerHeight() + buffer;
            elem.scrollTop(scroll);
            return scroll;
        } else {
            return elem[0].scrollHeight - elem.scrollTop() - 
                elem.innerHeight() + buffer;
        }
    }
    
    // Adds new messages to the bottom of the chat.
    // Deletes older messages if the user is not currently 
    // looking through old messages.
    function loadRecentMessages() {
        $.ajax({
            type: 'POST',
            url: 'get_recent_messages/',
            data: { 'conversationId': conversationId },
            success: function(data) {
                // User is not scrolling through old messages.
                let activeChat = !(scrollBottom($chatWindow) > 0);
                
                addNewMessages(data.messages);
                
                if (activeChat) {
                    deleteOldMessages();
                    scrollBottom($chatWindow, 0);
                }
            },
            complete: function(data) {
                // After waiting, check for new messages.
                if ($modal.is(':visible')) {
                    setTimeout(() => { loadRecentMessages(); }, 1000);
                }
            }
        });
    }
    
    // Adds message to the conversation.
    function sendMessage(e) {
        e.preventDefault();
        let $inputBox = $('#input-message');
        
        // Get message and ensure that it exists
        // (ie not empty or just whitespace)
        let message = $inputBox.val().trim();
        if (!message) {
            return;
        }
        
        // Send message to server.
        $.ajax({
            type: 'POST',
            url: 'submit_message/',
            data: { 'conversationId': conversationId, 'message': message },
            success: function(data) {
                $inputBox.val('');
                scrollBottom($chatWindow, 0);
            },
            error: function(data) {
                $content.append('<p>Error sending message.</p>');
            }
        });
    }
    
    // Updates modal window to show conversation details.
    // Starts short polling to server.
    // data: object containing name of conversation.
    function showConversation(data) {
        $content.children().remove();
        $modal.attr('data-conversation-id', conversationId);
        // Id used to inform other parts of page 
        // which conversation is active.
        
        let $conversationName = $('<h3>');
        $conversationName.text(data.name);
        $content.append($conversationName);
        
        let $settingsButton = $('<a>');
        $settingsButton.text('Settings');
        $settingsButton.attr('id', 'conversation-settings');
        $content.append($settingsButton);
        // modal-conversation-settings.js defines settings button click event.
        
        // Window to display messages.
        $chatWindow = $('<div>');
        $chatWindow.attr('id', 'chat-window');
        $chatWindow.attr('style', 'height:300px;overflow:scroll;'); // TO-DO: move to css
        $chatWindow.on('scroll', scrollChatWindow);
        $chatMessages = $('<ul>');
        $chatMessages.attr('id', 'chat-messages');
        $chatWindow.append($chatMessages);
        $content.append($chatWindow);
        loadRecentMessages();
        
        // Form used to compose and send messages.
        let $inputForm = $('<form>');
        let $inputBox = $('<input>');
        $inputBox.attr('type', 'text');
        $inputBox.attr('id', 'input-message');
        $inputBox.attr('placeholder', 'Message ' + data.name);
        $inputForm.append($inputBox);
        let $inputSubmit = $('<button>');
        $inputSubmit.text('Send');
        $inputForm.on('submit', sendMessage);
        $inputForm.append($inputSubmit);
        
        $content.append($inputForm);
        
        $modal.show();
    }
    
    // Get conversation details and displays it in the modal window.
    function getConversation() {
        $.ajax({
            type: 'POST', 
            url: 'get_conversation/',
            data: { 'conversationId': conversationId },
            success: showConversation,
            error: function(data) {
                $content.append('<p>Error loading connection details.</p>');
                $modal.show();
            }
        });
    }
    
    // Executes when a conversation in the unordered list is clicked.
    // Uses getConversation to get & display conversation details.
    function clickConversation(e) {
        e.preventDefault();
        conversationId = $(this).attr('data-conversation-id');
        
        getConversation();
    }
    
    $('ul').on('click', 'li a[data-conversation-id]', clickConversation);
});