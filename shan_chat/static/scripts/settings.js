$(function() {
    $settingsForm = $('#settingsForm');
    $passwordForm = $('#passwordForm'); 
   
    $settingsForm.on('submit', function(e) {
        e.preventDefault();
       
        let formData = new FormData($settingsForm[0]);
        
        $.ajax({
            type: 'POST',
            url: 'submit_profile/',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {
                $('#message').text("Profile update successful.");
            }
       });
   });
   
   $passwordForm.on('submit', function(e) {
        e.preventDefault();
        
        let formData = new FormData($passwordForm[0]);
        $.ajax({
            type: 'POST',
            url: 'submit_password/',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {
                $('#message').text("Password update successful.");
            },
            error: function(data) {
                $('#message').text("Password update unsuccessful - please try again.");
            },
            complete: function(data) {
                $passwordForm[0].reset();
            }
       });
   });
});