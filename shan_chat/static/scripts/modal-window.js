$(function() {
    // Get modal windows, apply various settings.
    let $modal = $('div.modal-window');
    $modal.hide();
    
    // Add empty content div.
    let $content = $('<div>');
    $content.addClass('modal-content');
    $modal.append($content);
    
    // Add close button.
    let $close = $('<a>');
    $close.addClass('modal-close');
    $close.html('<a>X</a>'); // change later
    $close.on('click', function(e) {
        // Reset content div and hide this modal window.
        e.preventDefault();
        let $this = $(this);
        let children = $this.find('.modal-content').children();
        children.remove();
        $this.parent().hide();
    });
    $modal.append($close);
});