from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.http import JsonResponse

from .models import Message, Conversation
from connections.models import Connection
from users.models import Profile

def index(request):
    """Home page for Shan-Chat."""
    # A logged-in user should never be shown the home page.
    if request.user.is_authenticated:
        return redirect('conversations:conversations')
    else:
        return render(request, 'conversations/index.html')
    
@login_required
def conversations(request):
    """Show all conversations."""
    profile = Profile.objects.filter(user=request.user)[0]
    conversations = Conversation.objects.filter(participants=profile)
    context = {"conversations": conversations}
    return render(request, 'conversations/conversations.html', context)

@login_required
def new_conversation(request):
    """Create a new conversation."""
    return render(request, 'conversations/new_conversation.html')

@login_required
def conversation(request, conversation_id):
    """Details of an individual conversation."""
    conversation = get_object_or_404(Conversation, id=conversation_id)
    participant = conversation.participants.filter(user=request.user)
    if len(participant) == 0:
        # Current user is not a participant in this conversation.
        raise Http404
        
    return render(request, 'conversations/conversation.html')

@login_required
def edit_conversation(request, conversation_id):
    """Edit a conversation."""
    conversation = get_object_or_404(Conversation, id=conversation_id)
    participant = conversation.participants.filter(user=request.user)
    if len(participant) == 0:
        # Current user is not a participant in this conversation.
        raise Http404
        
    return render(request, 'conversations/edit_conversation.html')

@login_required
@require_POST
def get_conversation(request):
    """Gets name of a conversation."""
    try:
        conversation = Conversation.objects.get(id=int(request.POST['conversationId']))
    except:
        return JsonResponse({}, status=500)
    participant = conversation.participants.filter(user=request.user)
    if len(participant) == 0:
        # Current user is not a participant in this conversation.
        return JsonResponse({}, status=404)
    
    result = { 'conversationId': conversation.id, 'name': conversation.name }
    # ADD MOST RECENT MESSAGES
    
    return JsonResponse(result)

@login_required
@require_POST
def get_recent_messages(request):
    """Gets 20 most-recent messages of a conversation."""
    try:
        conversation = Conversation.objects.get(id=int(request.POST['conversationId']))
    except:
        return JsonResponse({}, status=500)
    
    messages = Message.objects.filter(conversation=conversation).order_by(
        '-time_sent')[:20]
        
    results = []
    for message in messages:
        message_dict = { 'id': message.id,
                         'content': message.text,
                         'time_sent': message.time_sent,
                         'author_profile_id': message.author.id,
                         'author_display_name': message.author.display_name }
        results.append(message_dict)

    return JsonResponse({'messages': results})

@login_required
@require_POST
def get_older_messages(request):
    """Gets 10 most-recent messages older than the one specified by id."""
    try:
        conversation = Conversation.objects.get(id=int(request.POST['conversationId']))
        cutoff = Message.objects.get(id=int(request.POST['messageId']))
    except:
        return JsonResponse({}, status = 500)
    
    messages = Message.objects.filter(conversation=conversation).order_by('-time_sent')
    
    # Remove messages until either running out of messages
    # or the cutoff has been removed
    while(messages.count() > 0):
        check = messages[0].id == cutoff.id
        messages = messages[1:]
        if (check):
            break
    
    messages = messages[:10]
    
    results = []
    for message in messages:
        message_dict = { 'id': message.id,
                         'content': message.text,
                         'time_sent': message.time_sent,
                         'author_profile_id': message.author.id,
                         'author_display_name': message.author.display_name }
        results.append(message_dict)
    
    return JsonResponse({'messages': results})
    

@login_required
@require_POST
def submit_message(request):
    """Adds a message to the conversation."""
    try:
        conversation = Conversation.objects.get(id=int(request.POST['conversationId']))
    except:
        return JsonResponse({}, status=500)
    participant = conversation.participants.filter(user=request.user)
    if len(participant) == 0:
        # Current user is not a participant in this conversation.
        return JsonResponse({}, status=404)
    
    current_profile = Profile.objects.filter(user=request.user)[0]
    message = Message(text=request.POST['message'], author=current_profile, 
                      conversation=conversation)
    message.save()
    
    return JsonResponse({})