"""Defines URL patterns for conversations."""

from django.urls import path

from . import views

app_name = 'conversations'
urlpatterns = [
    # Home page.
    path('', views.index, name='index'),
    
    # Show all conversations.
    path('conversations/', views.conversations, name='conversations'),
    
    # Create a new conversation.
    path('conversations/new/', views.new_conversation, name='new_conversation'),
    
    # Single conversation.
    path('conversations/<int:conversation_id>/', views.conversation, 
        name='conversation'),
    
    # Edit a conversation.
    path('conversations/<int:conversation_id>/edit', views.edit_conversation, 
        name='edit_conversation'),
    
    # Background processes.
    path('conversations/get_conversation/', views.get_conversation, name='get_conversation'),
    path('conversations/get_recent_messages/', views.get_recent_messages, name='get_recent_messages'),
    path('conversations/get_older_messages/', views.get_older_messages, name='get_older_messages'),
    path('conversations/submit_message/', views.submit_message, name='submit_message'),
]