from django.db import models
from connections.models import Connection
from users.models import Profile

class Conversation(models.Model):
    """Model representing a conversation."""
    name = models.CharField(max_length=100, blank=True)
    participants = models.ManyToManyField(Profile)
    
    def __str__(self):
        """Return a string representation of the model."""
        desc = ""
        if len(self.name) > 0:
            desc += self.name + " - "
        for person in self.participants.all():
            desc += str(person) + ", "
        if len(self.participants.all()) > 0:
            desc = desc[0:-2]
        return desc

class Message(models.Model):
    """Model representing a message."""
    text = models.TextField()
    time_sent = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE)
    
    def __str__(self):
        """Return a string representation of the model."""
        time_format = "%Y/%m/%d %H:%M"
        return str(self.author) + " (" + self.time_sent.strftime(time_format) + "): " + self.text